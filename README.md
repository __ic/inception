Inception
=========

Experimental architecture and service to explore a system live data flow.

The goal is to "shape" sensory data to amene it for continuous processing and use for autonomy and programming.

Requirements
--------------------
* Any Raspberry Pi based system should be able to run the code. Tested on Pi 4 and Pi 3B+ only.
* Pi connected to the internet with at least one IPv4 address.
* Python 3.9 or above on the Pi.
* A moder browser, with support for WebGL. Tested on Firefox only.

Setup
-----
Assuming this repository is cloned on the Pi:

```
# Using VirtualEnv or similar is recommended before installing the dependencies.
pip install -r requirements.txt
./bin/run
```

Then in a browser: `http://address:8000/`, where `address` is the first IPv4 address of the Pi (can be seen with `ip -4 -br a`). This should display the interface menu.

Note: The code has a hardcoded IP address. At this point you need to change it manually in the code.

Usage
-----

The home page of the server offers two menus:

* *Trad* is a typical realtime representation of the "sensory" data, here CPU temperature, load, and disk usage.
* *Three* is a realtime 3D representation of the same "sensory" data.
