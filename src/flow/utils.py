from datetime import datetime, timezone
from typing import List

from fastapi import WebSocket

from gpiozero import CPUTemperature, DiskUsage, LoadAverage


class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket) -> None:
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket) -> None:
        self.active_connections.remove(websocket)

    async def broadcast(self, message: str) -> None:
        for connection in self.active_connections:
            await connection.send_json(message)


class DataProxy:
    def __init__(self):
        self.cpu_temperature = CPUTemperature()
        self.cpu_load = LoadAverage()
        self.disk_usage = DiskUsage()

    def all(self):
        return {
            "ts": int(datetime.now(timezone.utc).timestamp()),
            "ct": self.cpu_temperature.temperature,
            "du": self.disk_usage.usage,
            "cl": self.cpu_load.load_average,
        }
