import asyncio
import os
from typing import List

from fastapi import FastAPI, Request, WebSocket, WebSocketDisconnect
from fastapi.responses import HTMLResponse, ORJSONResponse
from fastapi.templating import Jinja2Templates

from flow.utils import ConnectionManager, DataProxy

app = FastAPI()
templates = Jinja2Templates(
    directory=os.path.join(os.path.abspath(os.path.dirname(__file__)), "templates")
)
manager = ConnectionManager()
data_proxy = DataProxy()
host_ip = os.environ.get("INCEPTION_HOST_IP", "127.0.0.1")

# @app.get("/", response_class=ORJSONResponse)
@app.get("/", response_class=HTMLResponse)
async def base(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})


@app.get("/basic", response_class=HTMLResponse)
async def base(request: Request):
    return templates.TemplateResponse("basic.html", {"request": request, "host_ip": host_ip})


@app.get("/three", response_class=HTMLResponse)
async def base(request: Request):
    return templates.TemplateResponse("three.html", {"request": request, "host_ip": host_ip})


@app.websocket("/live")
async def websocket_endpoint(websocket: WebSocket):
    await manager.connect(websocket)
    try:
        while True:
            await asyncio.sleep(1)
            await manager.broadcast(data_proxy.all())
    except WebSocketDisconnect:
        manager.disconnect(websocket)
    finally:
        await websocket.close(code=1000)
